# Database related files

`Mediatek_en.sql` should be mostly translated.

## Creating the database

Scripts for creating the database.
- `Mediatek.sql`
- `Mediatek_en.sql`
    - Same script as `Mediatek.sql`, but in English.  
Any changes to the structure of the database will most likely happen in the
English version first, as the Norwegian version is in use, and that database
will have to be altered before those changes can be implemented.

## VIEWS

Views are all located in `visning.sql`.  
List of views:
- `Studiepoeng_totalt`
    - Show sum of study points
- `Semesteroversikt`
    - Show book titles, course name, grades, semester (number and year)
- `Karakterutskrift`
    - Print grades
- `Karakterutskrift_antall`
    - Number of grades
- `Navn_tabeller`
    - Name of current tables
- `Alt_tmp`
    - Used for `Alt`
- `Alt`
    - Show information related to book, school and courses. "Everything".

## PL SQL, triggers

Triggers are located in `triggere.sql`.  
Contains two triggers, mostly for testing so far.  
The first trigger does no work, as triggers can't return a value. The second
trigger stores changes done to grades, and stores course code, the old grade,
new grade and when the change was made.
