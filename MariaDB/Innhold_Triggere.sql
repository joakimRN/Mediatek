-- Testing av triggere

-- Når en karakter blir lagt til i tabellen "Fag", skal totalt antall studiepoeng også skrives ut.
CREATE TRIGGER Vis_Studiepoeng_totalt
AFTER UPDATE ON Fag
FOR EACH ROW
SELECT * FROM Studiepoeng_totalt; -- Triggere kan ikke returnere en verdi.

-- En trigger som tar vare på gamle verdier for karakterer når karakter blir oppdatert
DELIMITER //
CREATE TRIGGER LoggKarakter
BEFORE UPDATE ON Fag
FOR EACH ROW
BEGIN
  DECLARE vKarakter CHAR(1);
  INSERT INTO Karakterlogg (Dato, Fagkode, NewKarakter, OldKarakter) VALUES (NOW(), OLD.Fagkode, NEW.Karakter, OLD.Karakter);
END; //
DELIMITER ;
-- Pr 2017-12-19 fungerer denne triggeren! Mulig vKarakter kan fjernes.
