# About "mediatek"  

A simple database over misc media, on different platforms.  
For my sake, the focus has been on books, where I have extended
that part to include school books, by linking them to courses,
semesters and grades.  
For now, there is no GUI for the database. Four options can be pursued:
1. GUI client written in Java
2. GUI client written in Python
3. Web-GUI, written in HTML5/CSS3/PHP
4. Web-GUI, written in HTML5/CSS3/Flask

Of these options, I guess I'll start with PHP or Flask/Python(3) first.


## Dependencies

You need either MySQL or MariaDB database.  


## Directories

### dokumentasjon
Contains documentation. So far there are only the ERD for the database.
The `.graphml`-file is for [yEd](https://www.yworks.com/products/yed).
The `.png` contains an image of the ERD.  

### MariaDB directory

Contains the SQL part of the project. The database in itself is a MariaDB database.  
The `.json`-file is purely for testing so far.  

### Java directory

Thinking about a GUI made in Java. Related files are stored here.  

### PHP directory

For creating a web interface/GUI, in addition or instead of the Java GUI.

### Python/Flask directory

To be added.


## Thoughts

Stuff related to testing should be moved to a [testing branch](https://gitlab.com/joakimRN/Mediatek/tree/Test).
