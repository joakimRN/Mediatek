
public class Skolebok_HSN {
	private Bok boken;
	private Fag faget;
	private Semestertabell semester;
	
	
	public Skolebok_HSN(Bok boken, Fag faget, Semestertabell semester) {
		this.boken = boken;
		this.faget = faget;
		this.semester = semester;
	}


	public Bok getBoken() {
		return boken;
	}


	public Fag getFaget() {
		return faget;
	}


	public Semestertabell getSemester() {
		return semester;
	}


	public void setBoken(Bok boken) {
		this.boken = boken;
	}


	public void setFaget(Fag faget) {
		this.faget = faget;
	}


	public void setSemester(Semestertabell semester) {
		this.semester = semester;
	}
	
	
}
