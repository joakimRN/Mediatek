
public class Fag {
	private String fagkode;
	private String fagnavn;
	private String karakter;
	private double studiepoeng;
	
	
	public Fag(String fagkode, String fagnavn, String karakter, double studiepoeng) {
		this.fagkode = fagkode;
		this.fagnavn = fagnavn;
		this.karakter = karakter;
		this.studiepoeng = studiepoeng;
	}


	public String getFagkode() {
		return fagkode;
	}


	public String getFagnavn() {
		return fagnavn;
	}


	public String getKarakter() {
		return karakter;
	}


	public double getStudiepoeng() {
		return studiepoeng;
	}


	public void setFagkode(String fagkode) {
		this.fagkode = fagkode;
	}


	public void setFagnavn(String fagnavn) {
		this.fagnavn = fagnavn;
	}


	public void setKarakter(String karakter) {
		this.karakter = karakter;
	}


	public void setStudiepoeng(double studiepoeng) {
		this.studiepoeng = studiepoeng;
	}


	@Override
	public String toString() {
		return "Fag [fagkode=" + fagkode + ", fagnavn=" + fagnavn + ", karakter=" + karakter + ", studiepoeng="
				+ studiepoeng + "]";
	}
	
	
	

}
