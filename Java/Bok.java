public class Bok {
    private long isbn;
    private String tittel;
    private int opplag;
    private String forfatter;
    private String forlag;
    private int antallSider;
    private String omslag;
    private String sjanger;

    public Bok(long isbn, String tittel, int opplag, String forfatter, String forlag, int antallSider, String omslag, String sjanger){
        this.isbn = isbn;
        this.tittel = tittel;
        this.opplag = opplag;
        this.forfatter = forfatter;
        this.forlag = forlag;
        this.antallSider = antallSider;
        this.omslag = omslag;
        this.sjanger = sjanger;
    }

    

    public long getIsbn() {
		return isbn;
	}



	public String getTittel() {
		return tittel;
	}



	public int getOpplag() {
		return opplag;
	}



	public String getForfatter() {
		return forfatter;
	}



	public String getForlag() {
		return forlag;
	}



	public int getAntallSider() {
		return antallSider;
	}



	public String getOmslag() {
		return omslag;
	}



	public String getSjanger() {
		return sjanger;
	}



	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}



	public void setTittel(String tittel) {
		this.tittel = tittel;
	}



	public void setOpplag(int opplag) {
		this.opplag = opplag;
	}



	public void setForfatter(String forfatter) {
		this.forfatter = forfatter;
	}



	public void setForlag(String forlag) {
		this.forlag = forlag;
	}



	public void setAntallSider(int antallSider) {
		this.antallSider = antallSider;
	}



	public void setOmslag(String omslag) {
		this.omslag = omslag;
	}



	public void setSjanger(String sjanger) {
		this.sjanger = sjanger;
	}



	@Override
    public String toString() {
        return "Bok{" +
                "isbn=" + isbn +
                ", tittel='" + tittel + '\'' +
                ", opplag=" + opplag +
                ", forfatter='" + forfatter + '\'' +
                ", forlag='" + forlag + '\'' +
                ", antallSider=" + antallSider +
                ", omslag='" + omslag + '\'' +
                ", sjanger='" + sjanger + '\'' +
                '}';
    }
}
