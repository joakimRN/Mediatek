import java.util.ArrayList;

public abstract class Mediatek_DB {
    private ArrayList<Bok> boker = new ArrayList<Bok>();
    private ArrayList<Fag> fagene = new ArrayList<Fag>();
    private ArrayList<Skolebok_HSN> skoleboker = new ArrayList<Skolebok_HSN>();
    private ArrayList<Semestertabell> semestre = new ArrayList<Semestertabell>();

    /*
     * Add methods
     */
    public void addBok(long isbn, String tittel, int opplag, String forfatter, String forlag, int antallSider, String omslag, String sjanger){
        boker.add(new Bok(isbn, tittel, opplag, forfatter, forlag, antallSider, omslag, sjanger));
    }

    public void addFag(String fagkode, String fagnavn, String karakter, double studiepoeng) {
        fagene.add(new Fag(fagkode, fagnavn, karakter, studiepoeng));
    }

    public void addSemester(String semester, String aar) {
        semestre.add(new Semestertabell(semester, aar));
    }

    public void addSkolebok(long isbn, String fagkoden, String semesteret) {
        Bok boken = finnBokISBN(isbn);
        Fag faget = finnFagKode(fagkoden);
        Semestertabell semester = finnSemester(semesteret);

        skoleboker.add(new Skolebok_HSN(boken, faget, semester));
    }

    /*
     * Find methods
     */

    // Find books
    public Bok finnBokISBN(long isbn) {
        for (Bok enBok : boker) {if (enBok.getIsbn() == isbn) return enBok;}
        return new Bok(-1, "Ikke funnet", 0, "", "", 0, "", ""); // Book not found
    }

    public Bok finnBokTittel(String tittelen) {
        for (Bok enBok : boker) {if (enBok.getTittel().equals(tittelen)) return enBok;}
        return new Bok(-1, "Ikke funnet", 0, "", "", 0, "", ""); // Book not found
    }


    // Find courses
    public Fag finnFagKode(String fagkoden) {
        for (Fag faget : fagene) {if (faget.getFagkode().equals(fagkoden)) return faget;}
        return new Fag(null, null, null, -1); // Course not found
    }

    public Fag finnFagNavn(String fagnavnet) {
        for (Fag faget : fagene) {if (faget.getFagkode().equals(fagnavnet)) return faget;}
        return new Fag(null, null, null, -1); // Course not found
    }


    // Find semester
    public Semestertabell finnSemester(String semesteret) {
        for (Semestertabell s : semestre) {if (s.getSemester().equals(semesteret)) return s;}
        return new Semestertabell(null, null);
    }


    // Connect to database

    // Disconnect from database
}
