import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MediatekGUI extends JFrame implements ActionListener {
    private JButton hentBoker = new JButton("Hent bøker");
    private JButton hentFag = new JButton("Heng fag");
    private JButton kobleTilDB = new JButton("Koble til DB");
    private JButton kobleFraDB = new JButton("Koble fra DB");

    // main()
    public static void main(String[] args) {
        MediatekGUI vinduet = new MediatekGUI("Tittel");
        vinduet.setLocation(300, 300);
        vinduet.setVisible(true);
    }

    public MediatekGUI(String tittel) {
        // Opprettelse av vinduet
        super.setTitle(tittel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLayout(new GridLayout(1,1));

        // Legger til paneler
        JPanel knappepanel = new JPanel();
        add(knappepanel, BorderLayout.CENTER);

        JPanel kontrollPanel = new JPanel();
        add(kontrollPanel, BorderLayout.NORTH);

        // Fyller panelene

        // Kontrollpanel
        kontrollPanel.setLayout(new GridLayout(1, 2));
        kontrollPanel.add(kobleTilDB);
        kontrollPanel.add(kobleFraDB);

        // Knappepanel
        knappepanel.setLayout(new GridLayout(5,1));
        knappepanel.add(new JLabel("Hente informasjon fra databasen:"));
        knappepanel.add(hentBoker);
        knappepanel.add(hentFag);

        pack(); // Må sjekkes
    }

    public void actionPerformed(ActionEvent event) {
        String valg  = event.getActionCommand();
    }
}
