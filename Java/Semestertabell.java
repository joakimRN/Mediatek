
public class Semestertabell {
	private String semester;
	private String aar;
	
	
	public Semestertabell(String semester, String aar) {
		this.semester = semester;
		this.aar = aar;
	}
	public String getSemester() {
		return semester;
	}
	public String getAar() {
		return aar;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public void setAar(String aar) {
		this.aar = aar;
	}
	
	
}
